import os
from app import create_app

# SECTION
# Crear la instancia de la app de Flask indicando
# dónde esta definidos los parámetros de configuración

settings_module = os.getenv('APP_SETTINGS_MODULE')
app = create_app(settings_module)