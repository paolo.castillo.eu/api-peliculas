# ¿Por qué un API REST con Flask?
Como ya sabrás, Flask es un framework para desarrollo web escrito en Python. Se puede utilizar para diversos tipos de aplicación, entre ellas, desarrollo de APIs. De hecho, es uno de los mejores frameworks de desarrollo para implementar este tipo de soluciones por lo sencillo que resulta y las facilidades que ofrece.

# Recursos

Sin embargo, antes de dar paso al tutorial vamos a repasar qué es un recurso en un API REST.

Un recurso es un objeto con un tipo, datos asociados, relaciones con otros recursos y un conjunto de métodos que operan en él. Es similar a una instancia de objeto en un lenguaje de programación orientado a objetos, con la importante diferencia de que solo se definen unos pocos métodos estándar para el recurso (correspondientes a los métodos HTTP GET, POST, PUT y DELETE).

# Estructura de la aplicación

```
+api-peliculas
|_+ app
  |_+ common
    |_ __init__.py
    |_ error_handling.py   # Utilidades para el manejo de errores
  |_+ films
    |_+ api
      |_ __init__.py
      |_ resources.py   # Endpoints del API
      |_ schemas.py     # Esquemas para serializar los modelos
    |_ __init__.py
    |_ models.py   # Modelos
  |_ __init__.py   # Configuración de la aplicación
  |_ db.py         # Configuración de la base de datos
  |_ ext.py        # Instanciación de las extensiones
|_+ config         # Directorio para la configuración
  |_ __init__.py
  |_ default.py    # Configuración por defecto
|_ entrypoint.py   # Crea la instancia de la app
```

# Extensiones a utilizar para implementar un API REST en Flask

- Flask Restful: Es una extensión que permite generar APIs REST muy fácilmente. Además, viene con un montón de utilidades.
- Flask SQLAlchemy: Para interactuar con la base de datos a través de su ORM. Puedes encontrar más información [aquí](https://j2logo.com/tutorial-flask-leccion-5-base-de-datos-con-flask-sqlalchemy/).
- Flask Migrate: Esta extensión permite generar las tablas de la base de datos a partir de ficheros de migración. Puedes encontrar más información [aquí](https://j2logo.com/tutorial-flask-leccion-11-actualizar-base-de-datos-sqlalchemy/).
- Flask Marshmallow: Es una extensión que facilita la serialización de los modelos de la base de datos a JSON y viceversa. Está basada en Marshmallow.
- Marshmallow SQLAlchemy: Para integrar Flask Marshmallow con SQLAlchemy.

Instalació de extensiones y dependencias:

```
$> pip install flask flask_restful flask_sqlalchemy flask_migrate flask_marshmallow marshmallow-sqlalchemy
```
