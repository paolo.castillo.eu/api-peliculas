# Control de errores de una API REST en Flask
# Mostrar un 404 en el caso de un json vacio o error al consultar

# NOTE
# Hereda todas las excepciones y errores de la aplicación
class AppErrorBaseClass(Exception):
    pass

# NOTE
# Se utilizara para cuando intente acceder a un recurso y no exista
class ObjectNotFound(AppErrorBaseClass):
    pass

