# Flask-SQLAlchemy : variable db 
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

# NOTE
# Esta vez sí instanciamos Flask-SQLAlchemy en la variable db. 
# Del mismo modo, lo hacemos aquí para evitar referencias circulares. 
# Además, se crea la clase BaseModelMixin con métodos de utilidad para los modelos.

class BaseModelMixin:
    def save(self):
        print(self)
        db.session.add(self)
        db.session.commit()
        
    def delete(self):
        db.session.delete(self)
        db.session.commit()
        
    @classmethod
    def get_all(cls):
        return cls.query.all()
    
    @classmethod
    def get_by_id(cls, id):
        return cls.query.get(id)
    
    @classmethod
    def simple_filter(cls, **kwargs):
        return cls.query.filter_by(**kwargs).all()