from app.db import db, BaseModelMixin

# SECTION
# Modelos de Film y Actor, en lo cual es la representación
# de una DB

# NOTE
# __repr__ : Devuelve un String con una representación del estado del objeto y 
# dicho String debe ser interpretado sin errores por Python.

# NOTE
# Film representa a una pelicula del catálogo
class Film(db.Model, BaseModelMixin):
    
    # NOTE
    # id, clave primaria.
    # title, título de la película.
    # length, duración (en segundos) de la película.
    # year, año de estreno.
    # director, director de la película.
    # actors, lista con los actores de la película.

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    length = db.Column(db.Integer)
    year = db.Column(db.Integer)
    director = db.Column(db.String)
    actors = db.relationship('Actor', backref='film', lazy=False, cascade='all, delete-orphan')

    def __init__(self, title, length, year, director, actors=[]):
        self.title = title
        self.length = length
        self.year = year
        self.director = director
        self.actors = actors
    
    def __repr__(self):
        return f'Film({self.title})'
    
    def __str__(self):
        return f'{self.title}'

# NOTE Actor representa a un actor
class Actor(db.Model, BaseModelMixin):
    
    # NOTE
    # id, clave primaria.
    # name, nombre del actor.
    # film_id, clave ajena a la película en la que aparece.
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    film_id = db.Column(db.Integer, db.ForeignKey('film.id'), nullable=False)

    def __init__(self, name):
        self.name = name
    
    def __repr__(self):
        return f'Actor({self.name})'
    
    def __str__(self):
        return f'{self.name}'