from marshmallow import fields
from app.ext import ma

# SECTION
# Para definir los esquemas utilizaremos Flask-Marshmallow. 
# Un esquema es una clase que define cómo se serializa un modelo/recurso consumido por el API a JSON.
# Un esquema también permite crear un modelo a partir de un JSON.

# Los esquemas se definen de manera muy similar a los modelos, solo que, para los campos, 
# hay que utilizar los tipos definidos por Marhsmallow.

# Cuando Marshmallow encuentre un campo en el esquema que coincida con el nombre 
# de un atributo en el modelo, tratará de serializarlo.


# NOTE
# dump_only=True : solo se toma encuenta a la hora de serializar el objeto
# y no en la carga
# Nested : Crear la instanción de actor, en lo cual puede contener


class FilmSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    title = fields.String()
    length = fields.Integer()
    year = fields.Integer()
    director = fields.String()
    actors = fields.Nested('ActorSchema', many=True)


class ActorSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String()