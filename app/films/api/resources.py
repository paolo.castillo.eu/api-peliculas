from app.common.error_handling import ObjectNotFound
from flask import request, Blueprint
from flask_restful import Api, Resource

from .schemas import FilmSchema
from ..models import Film, Actor

# SECTION
# Toda API REST utiliza protocolos HTTP&HTTPS y algunos de sus verbos
# Generalmente estos verbos, lo mas utilizados son:
# GET: Para obtener un recurso o colección.
# POST: Para crear un recurso (y/o añadirlo a una colección).
# PUT/PATCH: Para modificar un recurso.
# DELETE: Para eliminar un recurso.

# Para la implementación, se utilizara flask para la extensión de 
# flask-restfull. En un recurso no es mas que una clase asociado a un endpoint
# url que se expone como recurso.

# NOTE
# Crear como recurso:
# Colección de peliculas: Este recurso es en realidad una colección. Permite obtener
# el catálogo completo de peliculas y añadir un nuevo catalogo.
# Película: Este recurso obtendra una película del catálogo a partir de un ID


# NOTE
# Colección de Film Resource que todo los recurso de flask-resfull
# debe heredar de la clase resource
class FilmListResource(Resource):
    # get = petición /GET/ a la url /api/v1.0/films/
    def get(self):
        films = Film.get_all()
        result = film_schema.dumps(films, many=True)
        return result

    # post = petición /POSt/ a la url /api/v1.0/films/
    def post(self):
        # obtiene el cuerpo de JSON
        data = request.get_json()
        # load : valida que la data entrante 
        # cumpla con el esquema
        film_dict = film_schema.load(data)
        
        # almacena diccionario con los valores
        film = Film(title=film_dict['title'],
                    length=film_dict['length'],
                    year=film_dict['year'],
                    director=film_dict['director']
        )
        
        for actor in film_dict['actors']:
            film.actors.append(Actor(actor['name']))
        
        film.save()
        resp = film_schema.dump(film)
        return resp, 201
    
        
# NOTE
# Colección de Film para obtener peliculas
class FilmResource(Resource):
    def get(self, film_id):
        film = Film.get_by_id(film_id)
        if film is None:
            raise ObjectNotFound('La película no existe')
        resp = film_schema.dumps(film)
        return resp

# NOTE
# Blueprint crear recursos de otros paquetes
films_v1_0_bp = Blueprint('films_v1_0_bp', __name__)

# NOTE
# Crear instancia del esquema
film_schema = FilmSchema()

# NOTE
# Crear instancia de la API a partir de Blueprint 
# en lo cual expone metodos propios de una API de Flask-Resfull
api = Api(films_v1_0_bp)

api.add_resource(FilmListResource, '/api/v1.0/films/', endpoint='film_list_resource')
api.add_resource(FilmResource, '/api/v1.0/films/<int:film_id>', endpoint='film_resource')
