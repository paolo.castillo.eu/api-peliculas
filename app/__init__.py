from flask import Flask, jsonify
from flask_restful import Api

from app.common.error_handling import ObjectNotFound, AppErrorBaseClass
from app.db import db
from app.films.api.resources import films_v1_0_bp
from .ext import ma, migrate

# SECTION
# Implementación del método de crear la app de Flask. Ademas
# se registran los manejadores de errores para los códigos de respuestas
# y excepciones no controladas.

def register_error_handlers(app):
    @app.errorhandler(Exception)
    def handle_exception_error(e):
        return jsonify({'msg': 'Internal server error'}), 500
    @app.errorhandler(405)
    def handle_405_error(e):
        return jsonify({'msg': 'Method not allowed'}), 405
    @app.errorhandler(403)
    def handle_403_error(e):
        return jsonify({'msg': 'Forbidden error'}), 403
    @app.errorhandler(404)
    def handle_404_error(e):
        return jsonify({'msg': 'Not Found error'}), 404
    @app.errorhandler(AppErrorBaseClass)
    def handle_app_base_error(e):
        return jsonify({'msg': str(e)}), 500
    @app.errorhandler(ObjectNotFound)
    def handle_object_not_found_error(e):
        return jsonify({'msg': str(e)}), 404
    
def create_app(settings_module):
    app = Flask(__name__)
    app.config.from_object(settings_module)
    
    # inicializar las extensiones
    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)
    
    # Captura todo los errores 404
    Api(app, catch_all_404s=True)
    
    # Deshabilita el modo estricto de acabado de una URL con /
    app.url_map.strict_slashes = False
    
    # Registrar los blueprints
    app.register_blueprint(films_v1_0_bp)
    
    # Registrar manejadores de errores personalizados
    register_error_handlers(app)
    
    return app
